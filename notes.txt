Testing framework
Going for TestNG
http://blog.takipi.com/junit-vs-testng-which-testing-framework-should-you-choose/
http://blog.takipi.com/the-top-100-java-libraries-in-2016-after-analyzing-47251-dependencies/
https://javalibs.com/compare/junit-vs-testng

Using RESTlet
Not using jersey or glassfish because they are hosted ones which needs to create a war file and deploy

Documentation
https://restlet.com/open-source/documentation/javadocs/2.3/jse/api/

Test NG
http://testng.org/doc/documentation-main.html
http://testng.org/javadocs/index.html

Jenkins Started
http://localhost:8081/job/krupeshJenkin/job/userMessaging/build?token=makeUserMessage

Jenkins Server
java -jar jenkins.war --httpPort=8081

Start the user server
java -jar target/myapp-1.0-jar-with-dependencies.jar &

Kill the server
lsof -P | grep 'java.*:8080' | awk '{print $2}' | xargs kill -9

Final Jenkins Command execution
./build.sh &&
java -jar target/myapp-1.0-jar-with-dependencies.jar &
sleep 5 &&
mvn -q test &&
lsof -P | grep ':8080' | awk '{print $2}' | xargs kill -9 && echo "Test Complete"

Jasmine
https://jasmine.github.io/2.5/introduction
https://jasmine.github.io/2.5/node.html
https://jasmine.github.io/2.5/ajax.html

Extras
Jersey vs GlassFish
https://jersey.java.net/documentation/latest/user-guide.html#getting-started

More testing things we can do for frontend
Integration / automation testing - Selenium
Stress testing - we can use jMeter -->> http://jmeter.apache.org/usermanual/get-started.html#non_gui

http://api.wunderground.com/api/3df790519b76c29d/conditions/q/CA/San_Francisco.json
http://api.wunderground.com/api/3df790519b76c29d/conditions/q/37.8,-122.4.json

Visited Helpful links
http://www.programcreek.com/java-api-examples/index.php?api=org.restlet.representation.Representation
https://www.tutorialspoint.com/testng/testng_parameterized_test.htm
http://toolsqa.com/selenium-webdriver/testng-parameters-data-provider/
http://stackoverflow.com/a/992434
http://stackoverflow.com/a/16583477 -->> Find and kill process at a port
https://github.com/tonysneed/Demo.HelloTypeScript
http://stackoverflow.com/a/9003614 -->> HTTPS
