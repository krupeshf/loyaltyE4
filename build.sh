#!/bin/bash
cd "$(dirname "$0")"
echo "Building backend" && mvn clean -q && mvn verify -q -DskipTests &&
echo "Done"
