package com.loyaltyone.app;

import org.restlet.*;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.resource.Directory;
import org.restlet.util.Series;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;

public class UserServer {
    final static String connectionUrl = "jdbc:postgresql://localhost:5432/postgres?currentSchema=user_message&user=krupeshf&password=password";

    /**
     * Starting both type of server
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Component component = new Component();

        //Making the logging level in restlet as WARNING, so it does not log all the info of get and post requests
        component.getLogger().setLevel(Level.WARNING);

        setupPort(component, args);

        component.getDefaultHost().attach("/", getFrontendApplication());
        component.getDefaultHost().attach("/data", getRestApiApplication());

        // Start the component.
        component.start();

        System.out.println("\nUserServer is running now!\n==================================\n");
    }

    /**
     * Setting up the server on the default port 8080 or based on commandLineArgs
     * Both type of server will run on this port
     * It creates https from the jks file
     *
     * @param component
     * @param commandLineArgs
     */
    private static void setupPort(Component component, String commandLineArgs[]) {
        // by default run at 8080
        int port = 8080;
        String scheme = "https";

        if (commandLineArgs.length == 2) {
            try {
                // do it in a try block in case parsing integer fails
                port = Integer.valueOf(commandLineArgs[0]);
                scheme = commandLineArgs[1];
            } catch (Throwable e) { /* ignore -- already set to default value */ }
        }

        Server server;
        if (scheme.equalsIgnoreCase("http")) {
            server = component.getServers().add(Protocol.HTTP, port);
        } else {
            server = component.getServers().add(Protocol.HTTPS, port);
            Series<Parameter> parameters = server.getContext().getParameters();
            parameters.add("sslContextFactory", "org.restlet.engine.ssl.DefaultSslContextFactory");
            parameters.add("keyStorePath", "krupeshLoyalty.jks");
            parameters.add("keyStorePassword", "password");
            parameters.add("keyPassword", "password");
            parameters.add("keyStoreType", "JKS");
        }

        Client fileClient = component.getClients().add(Protocol.FILE);

        setupParams(server.getContext().getParameters());
        setupParams(fileClient.getContext().getParameters());

    }

    /**
     * Gets the frontend web server - located at frontend folder
     *
     * @return
     */
    private static Application getFrontendApplication() {

        final String rootAbs = "file:///" + Utils.canonicalPath("frontend");

        System.out.println("Frontend root path: " + rootAbs);
        Application fileServer = new Application() {
            @Override
            public Restlet createInboundRoot() {
                Directory directory = new Directory(getContext(), rootAbs);
                directory.setListingAllowed(true);
                setupParams(directory.getContext().getParameters());
                return directory;
            }

        };
        fileServer.setName("File Server");
        return fileServer;
    }

    /**
     * Gets the application server for REST Apis
     *
     * @return
     */
    private static Application getRestApiApplication() {
        Application restRouter = new RestApiRouter();
        restRouter.setName("Rest API");
        return restRouter;
    }

    /**
     * Setting up threads and queued requests
     *
     * @param params
     */
    static void setupParams(Series<Parameter> params) {
        params.set("minThreads", "200");
        params.set("lowThreads", "1000");
        params.set("maxThreads", "1000");
        params.set("maxQueued", "10000");
    }

    /**
     * Default connection to postgres database
     *
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(UserServer.connectionUrl);
    }
}
