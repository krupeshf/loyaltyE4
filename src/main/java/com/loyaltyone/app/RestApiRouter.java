package com.loyaltyone.app;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class RestApiRouter extends Application {

    @Override
    public synchronized Restlet createInboundRoot() {

        Router router = new Router(getContext());

        // Plain text
        router.attach("/text", PlainTextResource.class);

        return router;
    }
}
