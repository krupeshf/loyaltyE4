package com.loyaltyone.app;

import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Normally it is request part of {@link MessageNode}
 */

class PlainTextRequest {
    String text;
    String userName;
    Integer parentId;
    String city;
    Double latitude;
    Double longitude;
    Double temperature;

    // this is for gson conversion
    PlainTextRequest() {
    }

    public PlainTextRequest setText(String text) {
        this.text = text;
        return this;
    }

    public PlainTextRequest setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public PlainTextRequest setParentId(Integer parentId) {
        this.parentId = parentId;
        return this;
    }

    public PlainTextRequest setCity(String city) {
        this.city = city;
        return this;
    }

    public PlainTextRequest setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public PlainTextRequest setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public PlainTextRequest setTemperature(Double temperature) {
        this.temperature = temperature;
        return this;
    }
}

/**
 * {@link MessageNode#id} is message id
 * {@link MessageNode#message} is actual message text
 * {@link MessageNode#ts} is message timestamp when it was created - not when it was updated
 * {@link MessageNode#parentId} if not a root message then it will have a parentId
 * {@link MessageNode#cityName} is proper city name stored in database
 * {@link MessageNode#latitude} is latitude of the city
 * {@link MessageNode#longitude} is longitude
 * {@link MessageNode#temperature} is current temperature of city when message was created
 */

class MessageNode {
    int id;
    String message;
    String ts;
    int parentId;
    String cityName;
    double latitude;
    double longitude;
    double temperature;

    MessageNode() {
    }

    public MessageNode setId(int id) {
        this.id = id;
        return this;
    }

    public MessageNode setMessage(String message) {
        this.message = message;
        return this;
    }

    public MessageNode setTs(String ts) {
        this.ts = ts;
        return this;
    }

    public MessageNode setParentId(int parentId) {
        this.parentId = parentId;
        return this;
    }

    public MessageNode setCityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public MessageNode setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public MessageNode setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public MessageNode setTemperature(double temperature) {
        this.temperature = temperature;
        return this;
    }

}

class MessageResponse {
    List<MessageNode> messages;

    MessageResponse() {
    }

    MessageResponse(List<MessageNode> messages) {
        this.messages = messages;
    }
}

/**
 * PlainTextResource is for inserting and getting messages
 */
public class PlainTextResource extends RestfulResource {

    /**
     * Getting all the message details for the user
     * <p>
     * Each node will be {@link MessageNode}
     *
     * @return Returns {@link MessageResponse}, messages are returned in order of {@link MessageNode#id}
     * @throws ResourceException
     */
    @Override
    public Representation get() throws ResourceException {
        String userName = requireQuery("userName");
        // con, statement and rs are autoClosable so use it inside try block
        try (Connection con = UserServer.getConnection()) {
            try (Statement statement = con.createStatement()) {
                try (ResultSet rs = statement.executeQuery("select m.id, m.parent_id, m.message, m.ts, c.name as cityname, c.latitude, c.longitude, m.temperature from user_message.message as m left outer join user_message.cities as c on m.city_id = c.id where m.user_id = (select id from user_message.users where username = '" + userName + "') order by m.id")) {
                    List<MessageNode> messages = new ArrayList<>();
                    while (rs.next()) {
                        messages.add(new MessageNode()
                                .setId(rs.getInt("id"))
                                .setParentId(rs.getInt("parent_id"))
                                .setTs(DataUtils.getFrontendDateFormat(rs.getTimestamp("ts")))
                                .setMessage(rs.getString("message"))
                                .setCityName(rs.getString("cityname"))
                                .setLatitude(rs.getDouble("latitude"))
                                .setLongitude(rs.getDouble("longitude"))
                                .setTemperature(rs.getDouble("temperature"))
                        );
                    }
                    return Utils.jsonRepresentationFromObject(new MessageResponse(messages));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw invalidInput("Cannot fetch data for user: '" + userName + "'");
        }
    }

    /**
     * Post request to insert message on database
     *
     * @param entity request should be of {@link PlainTextRequest}
     * @return Plain text is returned from the request part {@link PlainTextRequest#text}
     * @throws ResourceException if text, or username is empty {@link #invalidInput(String)} or some database issue {@link SQLException}
     * @see PlainTextRequest
     */
    @Override
    public Representation post(Representation entity) throws ResourceException {
        PlainTextRequest reqObj = requireInput(entity, PlainTextRequest.class);
        if (Utils.isEmpty(reqObj.text, reqObj.userName)) {
            throw invalidInput("Text or username is empty");
        }
        try (Connection con = UserServer.getConnection()) {

            int userId = DataUtils.insertOrGetUserId(reqObj.userName, con);
            int cityId = DataUtils.insertOrGetCityId(reqObj.city, reqObj.latitude, reqObj.longitude, con);

            try (PreparedStatement st = con.prepareStatement("insert into message (message, user_id, parent_id, city_id, temperature) values (?, ?, ?, ?, ?)")) {
                short ctr = 0;
                st.setString(++ctr, reqObj.text);
                st.setInt(++ctr, userId);
                if (reqObj.parentId != null) {
                    st.setInt(++ctr, reqObj.parentId);
                } else {
                    st.setNull(++ctr, Types.INTEGER);
                }
                st.setInt(++ctr, cityId);
                st.setDouble(++ctr, reqObj.temperature);
                st.executeUpdate();
            }

            return plainTextResponse(reqObj.text);
        } catch (SQLException se) {
            se.printStackTrace();
            throw invalidInput(se.getMessage());
        }
    }

}
