package com.loyaltyone.app;

import org.restlet.Request;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * DataUtils is used for database read write util functions
 */
public class DataUtils {

    /**
     * Gets the userId based on userName
     *
     * @param userName
     * @param con      connection object - reusing
     * @return by default if not found then returns -1 otherwise the actual id
     */

    public static int getUserId(String userName, Connection con) {
        return getId("select id from users where username = '" + userName + "'", con);
    }

    /**
     * Gets the id if the sql query is already available
     *
     * @param sql
     * @param con
     * @return by default if not found then returns -1 otherwise the actual id
     */

    public static int getId(String sql, Connection con) {
        int id = -1;
        try (Statement statement = con.createStatement()) {
            try (ResultSet rs = statement.executeQuery(sql)) {
                if (rs.next()) {
                    id = rs.getInt("id");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static int getCityId(String cityName, Connection con) {
        return getId("select id from cities where name = '" + cityName + "'", con);
    }

    /**
     * When using get request, this will be used to get the value of a key
     *
     * @param key
     * @return String value of the request - not converting to other objects
     */
    public static String getQuery(String key) {
        // getting the current request value
        String value = Request.getCurrent().getResourceRef().getQueryAsForm().getFirstValue(key);
        if (Utils.isEmpty(value)) {
            return null;
        }
        return value.toString();
    }

    /**
     * jsonDataFormat is the json format used for javascript frontend
     */
    public static final String jsonDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    /**
     * If we have Timestamp it converts to frontend timestamp
     *
     * @param d {@link Timestamp}
     * @return
     */
    public static String getFrontendDateFormat(Timestamp d) { //to be compatible with what frontend understands it
        if (d != null) { //if date is not found in db, do not try to convert to any format, just send back null
            TimeZone tz = TimeZone.getTimeZone("UTC"); //XXX not sure how to use this time zone
            DateFormat df = new SimpleDateFormat(jsonDateFormat);
            df.setTimeZone(tz);
            return df.format(new java.util.Date(d.getTime())); //first convert the timestamp to util.Date and then to the specified format
        } else {
            return null;
        }
    }

    public static int insertUser(String userName, Connection con) throws SQLException {
        return insertSingleData("insert into users (username) values ('" + userName + "')", con);
    }

    /**
     * If sql query is available then get the key which is generated
     *
     * @param sql
     * @param con
     * @return
     * @throws SQLException either insertion fails or if key is not generated
     */

    public static int insertSingleData(String sql, Connection con) throws SQLException {
        int genId;
        try (Statement statement = con.createStatement()) {
            int insertedRow = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            if (insertedRow != 1) {
                throw new SQLException("Insertion failed: " + sql);
            }
            try (ResultSet rs = statement.getGeneratedKeys()) {
                if (rs.next()) {
                    genId = rs.getInt(1);
                } else {
                    throw new SQLException("Keys not generated for: " + sql);
                }
            }
        }
        return genId;
    }

    /**
     * Two functionality done in one function to make it easier to use
     *
     * @param userName
     * @param con
     * @return
     * @throws SQLException
     */
    public static int insertOrGetUserId(String userName, Connection con) throws SQLException {
        int userId = DataUtils.getUserId(userName, con);
        if (userId == -1) {
            userId = DataUtils.insertUser(userName, con);
        }
        return userId;
    }

    public static int insertCity(String cityName, Double latitude, Double longitude, Connection con) throws SQLException {
        return insertSingleData("insert into cities (name, latitude, longitude) values ('" + cityName + "', " + latitude + ", " + longitude + ")", con);
    }

    public static int insertOrGetCityId(String cityName, Double latitude, Double longitude, Connection con) throws SQLException {
        int cityId = DataUtils.getCityId(cityName, con);
        if (cityId == -1) {
            cityId = DataUtils.insertCity(cityName, latitude, longitude, con);
        }
        return cityId;
    }
}
