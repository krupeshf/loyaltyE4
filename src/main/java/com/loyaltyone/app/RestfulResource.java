package com.loyaltyone.app;

import com.google.gson.JsonObject;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import java.io.IOException;

public class RestfulResource extends ServerResource {

    /**
     * Utility to get JsonObject based on entity
     *
     * @param entity
     * @return
     * @throws ResourceException if entity was not json object then exception is thrown
     */
    protected JsonObject getInput(Representation entity) {
        try {
            return Utils.parseJsonObject(entity.getText());
        } catch (IOException e) {
            throw invalidInput("Entity input not formatted correctly");
        }
    }

    /**
     * If input is null then throws Exception
     *
     * @param entity
     * @return
     * @throws ResourceException if entity was not json object then exception is thrown
     * @see #getInput(Representation)
     */
    protected JsonObject requireInput(Representation entity) {
        JsonObject r = getInput(entity);
        if (r == null) {
            throw invalidInput("Json Request not formatted correctly");
        }
        return r;
    }

    /**
     * gets the {@link ResourceException} Bad request - error code 400
     *
     * @param message
     * @return
     */
    protected static ResourceException invalidInput(String message) {
        return abort(400, message);
    }

    /**
     * Different kind of aborting based on the error code
     *
     * @param code
     * @param message
     * @return
     */
    private static ResourceException abort(int code, String message) {
        return exception(code, message);
    }

    private static ResourceException exception(int code, String message) {
        System.err.println(message);
        Thread.dumpStack();
        return new ResourceException(makeStatus(code, message));
    }

    /**
     * Creates the status to be sent as response to frontend
     *
     * @param code
     * @param message
     * @return
     */
    private static Status makeStatus(int code, String message) {
        return new Status(code, "error", message);
    }

    protected static Representation plainTextResponse(String response) {
        return new StringRepresentation(response, MediaType.TEXT_PLAIN);
    }

    protected <T> T getInput(Representation entity, Class<T> cls) throws IOException {
        return Utils.parseJsonObject(entity.getText(), cls);
    }

    protected <T> T requireInput(Representation entity, Class<T> cls) {
        try {
            return Utils.parseJsonObject(entity.getText(), cls);
        } catch (IOException e) {
            throw invalidInput("Entity input not formatted correctly");
        }
    }

    /**
     * gets the value for the key in get request
     *
     * @param key
     * @return
     */
    protected String getQuery(String key) {
        return DataUtils.getQuery(key);
    }

    protected String requireQuery(String key) {
        String query = getQuery(key);
        if (Utils.isEmpty(query)) {
            throw invalidInput("Required key '" + key + "' is not found");
        }
        return query;
    }
}
