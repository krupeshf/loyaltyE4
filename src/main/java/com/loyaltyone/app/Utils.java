package com.loyaltyone.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

import java.io.File;
import java.io.IOException;

/**
 * Utils for all the utils used in the project
 */
public class Utils {

    /**
     * Get full canonicalPath based on relative path
     *
     * @param path
     * @return
     */
    public static String canonicalPath(String path) {
        try {
            return (new File(path)).getCanonicalPath();
        } catch (IOException e) {
            return path;
        }
    }

    /**
     * Coverting to {@link JsonObject} from the string
     *
     * @param jsonString
     * @return parsed jsonobject
     */
    public static JsonObject parseJsonObject(String jsonString) {
        // first convert to jsonElement and then send it back as JSON Object
        return new JsonParser().parse(jsonString).getAsJsonObject();
    }

    public static final Gson g = new GsonBuilder().create();

    /**
     * Generic function which will convert to desired class from the jsonstring
     *
     * @param jsonString
     * @param cls
     * @param <T>
     * @return Parsed Object
     */
    public static <T> T parseJsonObject(String jsonString, Class<T> cls) {
        return g.fromJson(jsonString, cls);
    }

    /**
     * Checking if list of strings are empty
     *
     * @param strings
     * @return true if string is empty or null otherwise false
     */
    public static boolean isEmpty(String... strings) {
        for (String s : strings) {
            if (stringEmpty(s)) {
                return true;
            }
        }
        return false;
    }

    private static boolean stringEmpty(String s) {
        return (s == null || s.length() == 0);
    }

    /**
     * Restlet {@link Representation} from the {@link JsonObject}
     *
     * @param jsonObject
     * @return
     */
    public static Representation jsonRepresentationFromJsonObject(JsonObject jsonObject) {
        return new StringRepresentation(jsonObject.toString(), MediaType.APPLICATION_JSON);
    }

    public static String jsonStringFromObject(Object obj) {
        return g.toJson(obj);
    }

    public static JsonObject jsonObjectFromObject(Object obj) {
        return new JsonParser().parse(jsonStringFromObject(obj)).getAsJsonObject();
    }

    public static Representation jsonRepresentationFromObject(Object obj) {
        return new StringRepresentation(jsonStringFromObject(obj), MediaType.APPLICATION_JSON);
    }

    /**
     * Creating url based as scheme host or port
     *
     * @param httpScheme
     * @param host
     * @param port
     * @return string url
     */
    public static String buildUrl(String httpScheme, String host, String port) {
        return httpScheme + "://" + host + ":" + port;
    }
}
