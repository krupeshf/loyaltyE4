package com.loyaltyone.app;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.Statement;

/**
 * Migration utility to migrate previous version of database to next version
 */
public class Migrate {
    public static void main(String[] args) {
        File[] sqlFiles = new File("migrations").listFiles();
        try (Connection con = UserServer.getConnection()) {
            con.setAutoCommit(false);
            for (File f : sqlFiles) {
                System.out.println(f.toPath().toString());
                String content = new String(Files.readAllBytes(f.toPath()));
                try (Statement statement = con.createStatement()) {
                    statement.execute(content);
                }
            }
            con.commit();
            System.out.println("Done");
        } catch (Throwable e) {
            System.out.println();
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
            System.out.println("\nMigrations not applied");
            System.exit(4);
        }
    }
}
