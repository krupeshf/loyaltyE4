package com.loyaltyone.app;

import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.Protocol;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.sql.*;

public class PlainTextTest {

    private String postUrl = "";
    private Connection connection;
    public Timestamp ts;
    private ClientResource clientResource;
    private final static String USER_NAME = "testngUser";
    private final static String UNKOWN_CITY = "noCityOnPlanteEarth";

    @BeforeClass
    @Parameters({"httpScheme", "host", "port"})
    public void init(String scheme, String host, String port) {
        this.postUrl = Utils.buildUrl(scheme, host, port) + "/data/text";

        Client client = new Client(new Context(), Protocol.HTTP);
        this.clientResource = new ClientResource(this.postUrl);
        this.clientResource.setNext(client);

        try {
            this.connection = UserServer.getConnection();
            this.ts = new Timestamp(new java.util.Date().getTime());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail("Cannot connect to database");
        }
    }

    @Test(dataProvider = "testDataProvider")
    public void testPrintMessage(PlainTextRequest requestObj, String matchedString, boolean success) {

        String returnString = null;
        try {
            this.clientResource.post(Utils.jsonRepresentationFromObject(requestObj));
            returnString = this.clientResource.getResponseEntity().getText();
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail("Post unsuccessful", e);
        } catch (ResourceException e) {
            if (success) {
                Assert.fail("Post unsuccessful", e);
            }
        }

        if (success) {
            Assert.assertEquals(returnString, matchedString);
            try (Statement statement = this.connection.createStatement()) {
                ResultSet rs = statement.executeQuery("select message from message where message = " + requestObj.text);
                if (!rs.next()) {
                    Assert.fail("Data not inserted");
                }
            } catch (SQLException se) {
            }
        } else {
            Assert.assertNotEquals(returnString, matchedString);
        }
    }

    @DataProvider(name = "testDataProvider")
    public static Object[][] getTestCases() {
        return new Object[][]{
                {new PlainTextRequest().setText("hello").setUserName(PlainTextTest.USER_NAME).setCity("Rajkot, Gujarat, India").setTemperature(5.5), "hello", true},
                {new PlainTextRequest().setText("hello").setUserName(PlainTextTest.USER_NAME).setCity(PlainTextTest.UNKOWN_CITY).setTemperature(5.5), "hello", false},
                {new PlainTextRequest().setText("hello").setUserName(PlainTextTest.USER_NAME).setCity(PlainTextTest.UNKOWN_CITY).setLatitude(-91d).setLongitude(181d).setTemperature(5.5), "hello", false},
                {new PlainTextRequest().setText("hello").setUserName(PlainTextTest.USER_NAME).setCity(PlainTextTest.UNKOWN_CITY).setLatitude(10d).setLongitude(55d).setTemperature(5.5), "hello", true},
                {new PlainTextRequest().setText("hello").setUserName(PlainTextTest.USER_NAME).setCity("Rajkot, Gujarat, India").setTemperature(12d), "bello", false},
                {new PlainTextRequest(), "heo", false},
        };
    }

    @AfterClass
    public void clearData() {
        try {
            try (Statement statement = connection.createStatement()) {
                // directly deleting the data related to testng user this way all the related messages will be deleted
                statement.executeUpdate("delete from users where username = '" + PlainTextTest.USER_NAME + "'");

                statement.executeUpdate("delete from cities where name = '" + PlainTextTest.UNKOWN_CITY + "'");
            }
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail("Cannot rollback or close connection");
        }
    }

}
