create table if not exists users (
    id serial primary key,
    username character varying not null unique
)
