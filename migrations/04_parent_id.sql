DO $$
    BEGIN
        BEGIN
            ALTER TABLE message ADD column parent_id integer DEFAULT null references message (id) on delete cascade on update cascade;
        EXCEPTION WHEN duplicate_column THEN END;
    END;
$$

