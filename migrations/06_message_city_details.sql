DO $$
    BEGIN
        BEGIN
            ALTER TABLE message ADD COLUMN city_id int references cities (id) on delete cascade on update cascade;
            ALTER TABLE message ADD COLUMN temperature double precision;
        EXCEPTION WHEN duplicate_column THEN END;
    END;
$$

