-- http://www.postgresql.org/docs/9.4/static/sql-altertable.html
-- does not have IF NOT EXISTS for adding column
-- http://stackoverflow.com/a/12608570
DO $$
    BEGIN
        BEGIN
            ALTER TABLE message ADD COLUMN user_id int references users (id) on delete cascade on update cascade NOT NULL DEFAULT 0;
        EXCEPTION WHEN duplicate_column THEN END;
    END;
$$

