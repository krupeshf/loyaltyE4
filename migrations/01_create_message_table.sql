create table if not exists message (
    id serial primary key,
    message text not null,
    ts timestamp default current_timestamp not null
)
