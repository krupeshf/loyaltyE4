create table if not exists cities (
    id serial primary key,
    name character varying not null unique,
    latitude double precision not null,
    longitude double precision not null
)
