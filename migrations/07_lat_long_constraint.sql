ALTER TABLE cities DROP CONSTRAINT IF EXISTS cities_latitude_cons;
ALTER TABLE cities ADD CONSTRAINT cities_latitude_cons CHECK (latitude >= -90 AND latitude <= 90 AND longitude >= -180 AND longitude <= 180);
