import React = require("react");

export function stopDefaultAndPropogation(event: Event | React.SyntheticEvent<HTMLElement>) {
    event.preventDefault();
    event.stopPropagation();

    const cancelBubble = (event as any).cancelBubble;
    if (cancelBubble) {
        // for other browsers if stopPropagation does not work
        (event as any).cancelBubble = true;
    }
}