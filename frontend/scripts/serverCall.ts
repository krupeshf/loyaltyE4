import jquery = require("jquery");

declare var require: any;

import "rsvp";
let RSVP = require("rsvp");
let Promise = RSVP.Promise;

type RestCallType = "GET" | "POST" | "PUT" | "DELETE";

export class ServerCall {
    static setWindowGetApi() {
        (window as any).getApi = function (url: string, data: any) {
            ServerCall.restCall("GET", url, data).then(function (response) {
                console.log(JSON.stringify(response, null, 4));
                (window as any)._get_result = response;
            });
        };
    }
    static restCall<ServerRequest, ServerResponse>(type: RestCallType, url: string, data: ServerRequest, crossDomain: boolean = false): Promise<ServerResponse> {
        if (data === undefined || data === null) {
            data = {} as ServerRequest;
        }
        let settings: JQueryAjaxSettings = {
            url: url,
            type: type,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            cache: false,
            crossDomain: true,
        };
        if (crossDomain) {
            settings.dataType = "jsonp";
        }
        if (type === "GET") {
            let param = jquery.param(data);
            if (param) {
                settings.url += "?" + param;
            }
            delete settings.data;
        }
        return new Promise(function (resolve: (data: any, textStatus: string, jqXHR: JQueryXHR) => any, reject: any) {
            settings.success = resolve;
            settings.error = (xhr: JQueryXHR, textStatus: string, errorThrown: string) => {
                console.error(xhr.status, xhr.responseText);
                if (xhr.responseJSON) {
                    reject(xhr.responseJSON);
                } else {
                    reject({ error: xhr.responseText });
                }
            };

            jquery.ajax(settings);

        });
    };

    static weatherDataCall(latitude: number, longitude: number): Promise<number> {
        return ServerCall.restCall("GET", "https://api.wunderground.com/api/3df790519b76c29d/conditions/q/" + latitude + "," + longitude + ".json", {}, true).then((response: WeatherResponse) => {
            return response.current_observation.temp_c;
        });
    }
}

interface WeatherResponse {
    current_observation: {
        temp_c: number;
        temp_f: number;
    }
}

ServerCall.setWindowGetApi();

export default ServerCall;