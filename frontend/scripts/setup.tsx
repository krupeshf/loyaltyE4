import React = require("react");
import ReactDom = require("react-dom");
import { SimpleForm } from "simpleForm";

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////

export class UserMessagingSystem {
    public reactInit() {
        // initialzing the react component
        let reactDiv = document.getElementById("reactDiv");
        ReactDom.render(<SimpleForm />, reactDiv);
    }
}

let sys: UserMessagingSystem = new UserMessagingSystem();
sys.reactInit();