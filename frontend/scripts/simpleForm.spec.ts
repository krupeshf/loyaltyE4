/// <reference path="../typings/globals/jasmine/index.d.ts" />
import SimpleForm = require("simpleForm");
class ABC {
    constructor() {
        new SimpleForm.SimpleForm(null);
        describe("Greeter", () => {
            describe("greet", () => {
                it("returns Hello World", () => {
                    const result = "Hello World";
                    expect(result).toEqual("Hello World");
                });
            });
        });

    }
}