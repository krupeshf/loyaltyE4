import React = require("react");
import ReactDom = require("react-dom");

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////

// just having default props as empty which may be extended for future
interface HelloWorldProps {
}

interface HelloWorldState {
}

export class HelloWorld extends React.Component<HelloWorldProps, HelloWorldState> {
    constructor(props: HelloWorldProps) {
        super(props);
    }
    render() {
        return (
            <div>Hello World</div>
        );
    }
}

let HelloWorldDefaultProps: HelloWorldProps = {};
(HelloWorld as any).defaultProps = HelloWorldDefaultProps;

// when importing directly default Component should be HelloWorld Component
export default HelloWorld;