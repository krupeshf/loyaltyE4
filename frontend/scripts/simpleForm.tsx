import React = require("react");
import ReactDom = require("react-dom");
import { ServerCall } from "serverCall";
import Utils = require("utils");
import u = require("lodash");

declare var require: any;

import "google.maps";
let gMaps = require("google.maps");

let RSVP = require("rsvp");
let Promise = RSVP.Promise;

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////

interface PlainTextUserInfo extends CityDetails {
    text: string;
    userName: string;
    parentId?: number;
}

interface PlainMessageNode {
    id: number;
    message: string;
    ts: string;
    parentId: number;
    cityName: string;
    latitude: number;
    longitude: number;
    temperature: number;
}

interface PlainTextUserMessages {
    messages: PlainMessageNode[];
}

interface TextNode extends PlainMessageNode {
    children?: TextNode[];
}

interface ParsedMessageList {
    messages: TextNode[];
}

interface CityDetails {
    latitude: number;
    longitude: number;
    temperature: number;
    city: string;
}

function parseMessages(serverMessages: PlainTextUserMessages): ParsedMessageList {
    return { messages: getChildren(serverMessages.messages, 0, 0) };
}

// passing startingPoint because this way we do not need to traverse the whole list everytime
function getChildren(messageList: PlainMessageNode[], id: number, startingPoint: number): TextNode[] {
    let childrenList: TextNode[] = [];
    let childNode: TextNode;
    for (let i = startingPoint; i < messageList.length; ++i) {
        if (messageList[i].parentId === id) {
            childNode = messageList[i];
            childNode.children = getChildren(messageList, childNode.id, i + 1);
            childrenList.push(childNode);
        }
    }
    return childrenList;
}

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////

interface SimpleFormProps {
}

interface SimpleFormState {
    responseText: string;
    userMessages: PlainTextUserMessages;
}

export class SimpleForm extends React.Component<SimpleFormProps, SimpleFormState> {
    public messageElement: HTMLInputElement;
    public userNameElement: HTMLInputElement;
    public cityElement: HTMLInputElement;
    public geocoder: google.maps.Geocoder;
    constructor(props: SimpleFormProps) {
        super(props);
        this.geocoder = new google.maps.Geocoder();
        this.state = {
            responseText: null,
            userMessages: null,
        }
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleOnMessagFetch = this.handleOnMessagFetch.bind(this);
        this.handleMessageReply = this.handleMessageReply.bind(this);
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleOnSubmit}>
                    <label>User City</label><input ref={(comp) => this.cityElement = comp} placeholder="Enter City Name" />
                    <br />
                    <label>User Name</label> <input ref={(comp) => this.userNameElement = comp} placeholder="Enter UserName" /> <button onClick={this.handleOnMessagFetch}>Fetch Messages</button>
                    <br />
                    <label>Root Message</label><input ref={(comp) => this.messageElement = comp} placeholder="Enter Text" /> <button type="submit">Send</button>
                    {this.state.responseText &&
                        <div style={{ marginTop: 10 }}>
                            <label>
                                Response -->> &nbsp;
                            {this.state.responseText}
                            </label>
                        </div>
                    }
                </form>
                <br />
                <MessageList {...this.state.userMessages} onMessageReplied={this.handleMessageReply} />
            </div>
        );
    }

    handleOnSubmit(event: React.SyntheticEvent<HTMLElement>) {
        Utils.stopDefaultAndPropogation(event);
        this.handleMessageReply(this.messageElement.value);
    }

    getCityDetails(): Promise<CityDetails> {
        return new Promise((resolve: (response: CityDetails) => void, reject: any) => {
            this.geocoder.geocode({ "address": this.cityElement.value }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    const latitude = results[0].geometry.location.lat();
                    const longitude = results[0].geometry.location.lng();
                    const city = results[0].formatted_address;
                    ServerCall.weatherDataCall(latitude, longitude).then((temperature) => {
                        const temp_c = temperature;
                        resolve({ latitude: latitude, longitude: longitude, temperature: temp_c, city: city });
                    }).catch((error: any) => {
                        reject("Something got wrong-> " + error);
                    });
                } else {
                    reject("Something got wrong-> " + status);
                }
            });
        });
    }

    handleMessageReply(message: string, parentId?: number) {
        this.setState({ responseText: null } as SimpleFormState);
        if (message && message.length > 0 && this.userNameElement.value.length > 0 && this.cityElement.value.length > 0) {
            this.getCityDetails().then((cityDetails: CityDetails) => {
                const messageRequest: PlainTextUserInfo = {
                    userName: this.userNameElement.value,
                    text: message,
                    parentId: parentId,
                    latitude: cityDetails.latitude,
                    longitude: cityDetails.longitude,
                    temperature: cityDetails.temperature,
                    city: cityDetails.city,
                };
                this.sendMessageToServer(messageRequest);
            }).catch((error: any) => {
                console.error(error);
            });
        }
    }

    private sendMessageToServer(messageRequest: PlainTextUserInfo) {
        this.setState({ responseText: null } as SimpleFormState);
        ServerCall.restCall("POST", "/data/text", messageRequest).then((responseStr: string) => {
            this.setState({ responseText: responseStr } as SimpleFormState);
            this.fetchUserMessages();
        });
    }

    handleOnMessagFetch(event: React.SyntheticEvent<HTMLElement>) {
        Utils.stopDefaultAndPropogation(event);
        this.fetchUserMessages();
    }

    private fetchUserMessages() {
        if (this.userNameElement.value.length > 0) {
            ServerCall.restCall("GET", "/data/text", { userName: this.userNameElement.value }).then((userMessages: PlainTextUserMessages) => {
                this.setState({ userMessages: parseMessages(userMessages) } as SimpleFormState);
            });
        }
    }
}

let SimpleFormDefaultProps: SimpleFormProps = {};
(SimpleForm as any).defaultProps = SimpleFormDefaultProps;

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////

interface MessageListProps extends ParsedMessageList {
    onMessageReplied: (message: string, parentId: number) => void;
}

interface MessageListState {
}

class MessageList extends React.Component<MessageListProps, MessageListState> {
    constructor(props: MessageListProps) {
        super(props);
    }
    render(): JSX.Element {
        return (
            <ul style={{ listStyleType: "none" }}>
                {this.props.messages && this.props.messages.length > 0 &&
                    this.props.messages.map((msg, index) => <ReactMessageNode key={index} {...msg} onMessageReplied={this.props.onMessageReplied} />)}
            </ul>
        );
    }
}

let MessageListDefaultProps: MessageListProps = { messages: [], onMessageReplied: () => { } };
(MessageList as any).defaultProps = MessageListDefaultProps;

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////


interface ReactMessageNodeProps extends TextNode {
    onMessageReplied: (message: string, parentId: number) => void;
}

interface ReactMessageNodeState {
    replyMessage: string;
}

class ReactMessageNode extends React.Component<ReactMessageNodeProps, ReactMessageNodeState> {
    public messageReplyBox: HTMLInputElement;
    constructor(props: ReactMessageNodeProps) {
        super(props);
        this.state = {
            replyMessage: null,
        }
        this.handleMessageReplied = this.handleMessageReplied.bind(this);
        this.handleOnReplyMessageChange = this.handleOnReplyMessageChange.bind(this);
    }
    render() {
        return (
            <li key={this.props.id}>
                {this.props.message}&nbsp;&nbsp;--&nbsp;&nbsp;
                {this.props.cityName}&nbsp;&nbsp;--&nbsp;&nbsp;
                ({this.props.latitude})&nbsp;&nbsp;--&nbsp;&nbsp;
                ({this.props.longitude})&nbsp;&nbsp;--&nbsp;&nbsp;
                ({this.props.temperature})&nbsp;&nbsp;--&nbsp;&nbsp;
                <input value={this.state.replyMessage}
                    ref={(comp) => this.messageReplyBox = comp}
                    onChange={this.handleOnReplyMessageChange}
                    type="text" placeholder="Reply Text"></input>
                <button onClick={this.handleMessageReplied}>Reply</button>
                {this.props.children && this.props.children.length > 0 && <MessageList messages={this.props.children} onMessageReplied={this.props.onMessageReplied} />}
            </li>
        );
    }
    private handleMessageReplied() {
        this.props.onMessageReplied(this.state.replyMessage, this.props.id);
        this.setState({ replyMessage: null } as ReactMessageNodeState);
    }

    handleOnReplyMessageChange() {
        this.setState({ replyMessage: this.messageReplyBox.value } as ReactMessageNodeState);
    }
}

// let ReactMessageNodeDefaultProps: ReactMessageNodeProps = {};
// (ReactMessageNode as any).defaultProps = ReactMessageNodeDefaultProps;

/////////////////////////////////////////////////////--/////////////////////////////////////////////////////////////

export default SimpleForm;