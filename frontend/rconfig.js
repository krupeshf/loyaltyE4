require.config({
    "baseUrl": "scripts",
    "paths": {
        "react": "../lib/react",
        "react-dom": "../lib/react-dom",
        "jquery": "../lib/jquery",
        "rsvp": "../lib/rsvp",
        "lodash": "../lib/lodash",
        "google.maps": "../lib/google.maps",
    },
    jsx: {
        fileExtension: ".js",
    }
});
console.debug("rconfig done");